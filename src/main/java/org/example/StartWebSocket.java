package org.example;

import io.quarkus.security.identity.CurrentIdentityAssociation;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.websocket.*;
import jakarta.websocket.server.ServerEndpoint;

import java.security.Principal;
import java.util.Optional;

@ApplicationScoped
@ServerEndpoint("/awesome-websocket")
@RolesAllowed("admin")
public class StartWebSocket {

    @Inject
    CurrentIdentityAssociation identity;

    @OnOpen
    public void onOpen(Session session) {
        String welcomeMessage = String.format("Welcome %1s!", resolveName());
        System.out.println("onOpen> " + welcomeMessage);
        System.out.println("onOpen> " + session.getId() + "has been opened");
        session.getAsyncRemote().sendText(welcomeMessage);
    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("onClose> " + session.getId() + "has been closed");
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        System.out.println("onError> " + throwable);
    }

    @OnMessage
    public void onMessage(Session session, String message) {
        String name = resolveName();
        System.out.println("onMessage> " + name + ": " + message);
    }

    private String resolveName() {
        return Optional.ofNullable(identity)
                .map(CurrentIdentityAssociation::getIdentity)
                .map(SecurityIdentity::getPrincipal)
                .map(Principal::getName)
                .orElse("username not resolved");
    }
}
